package ru.t1.gorodtsova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.gorodtsova.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public final class TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDTO> implements ITaskDtoRepository {

    public TaskDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m";
        return entityManager.createQuery(jpql, TaskDTO.class).getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId, @NotNull final Comparator<TaskDTO> comparator) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId ORDER BY :sort";
        return entityManager
                .createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("sort", getSortedColumn(comparator))
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull final String id) {
        return entityManager.find(TaskDTO.class, id);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT m FROM TaskDTO m WHERE m.userId = :userId AND m.projectId = :projectId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeAll() {
        @NotNull final String jpql = "DELETE FROM TaskDTO";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM TaskDTO m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM TaskDTO m";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setMaxResults(1)
                .getFirstResult();
    }

    @Override
    public int getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM TaskDTO m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, TaskDTO.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getFirstResult();
    }

}
