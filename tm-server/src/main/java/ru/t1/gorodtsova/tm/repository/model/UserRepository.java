package ru.t1.gorodtsova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.repository.model.IUserRepository;
import ru.t1.gorodtsova.tm.model.User;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final String jpql = "SELECT m FROM User m";
        return entityManager.createQuery(jpql, User.class).getResultList();
    }

    @Nullable
    @Override
    public User findOneById(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.login = :login";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT m FROM User m WHERE m.email = :email";
        return entityManager.createQuery(jpql, User.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

    @Override
    public void removeAll() {
        @NotNull final String jpql = "DELETE FROM User";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM User m";
        return entityManager.createQuery(jpql, User.class)
                .setMaxResults(1)
                .getFirstResult();
    }

}
