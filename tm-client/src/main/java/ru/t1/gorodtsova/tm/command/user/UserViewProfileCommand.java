package ru.t1.gorodtsova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.user.UserProfileRequest;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.exception.entity.UserNotFoundException;
import ru.t1.gorodtsova.tm.dto.model.UserDTO;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String DESCRIPTION = "View profile of current user";

    @NotNull
    private final String NAME = "view-user-profile";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @Nullable final UserDTO user = getUserEndpoint().viewProfileUser(request).getUser();
        if (user == null) throw new UserNotFoundException();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
